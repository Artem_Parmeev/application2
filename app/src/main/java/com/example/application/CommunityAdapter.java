package com.example.application;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.application.constants.DBConstants;

import java.util.ArrayList;

/**
 * Класс, представляющий собой адаптер для интерфейсного
 * компонента RecyclerView и инициализирующий необходимые
 * данные для отображения библиотеки
 *
 * @author Parmeev A.V. 17IT17
 */
public class CommunityAdapter extends RecyclerView.Adapter<CommunityAdapter.ProfileHolder> implements Filterable, DBConstants {

    private SQLiteDatabase db;
    private Context context;
    private SparseArray<String> names;
    private SparseArray<String> emails;
    private SparseArray<String> dates;
    private ArrayList<Integer> ids;
    private ArrayList<Integer> idsFull;
    private SparseArray<String> imgs;
    private AppCompatActivity activity;
    private String[] args;
    private String query;

    public CommunityAdapter(Context context, SQLiteDatabase db, String query, String[] args, AppCompatActivity activity) {
        this.context = context;
        this.db = db;
        this.query = query;
        this.args = args;
        this.activity = activity;
        initData();
    }

    /**
     * Инициализирует необходимые данные для
     * их последующего отображения на экране пользователя
     */
    private void initData() {
        names = new SparseArray<>();
        emails = new SparseArray<>();
        dates = new SparseArray<>();
        ids = new ArrayList<>();
        imgs = new SparseArray<>();

        Cursor cursor = db.rawQuery(query, args);
        if (cursor.moveToFirst()) {
            int nameIndex = cursor.getColumnIndex(DATABASE_NAME);
            int emailIndex = cursor.getColumnIndex(DATABASE_EMAIL);
            int registrationIndex = cursor.getColumnIndex("registrationDate");
            int idIndex = cursor.getColumnIndex(DATABASE_ID);
            int id;
            do {
                if (cursor.getString(emailIndex).equals(context.getSharedPreferences("AccData", Context.MODE_PRIVATE).getString(DATABASE_EMAIL, null))) {
                    return;
                }
                id = cursor.getInt(idIndex);
                names.put(id, cursor.getString(nameIndex));
                emails.put(id, cursor.getString(emailIndex));
                dates.put(id, cursor.getString(registrationIndex));
                ids.add(id);
            } while (cursor.moveToNext());
        }
        cursor.close();
        idsFull = new ArrayList<>(ids);
    }

    @Override
    public Filter getFilter() {
        return bookFilter;
    }

    private Filter bookFilter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            ids.clear();
            if (constraint == null || constraint.length() == 0) {
                ids.addAll(idsFull);
            } else {
                int id;
                for (int i = 0; i < names.size(); i++) {
                    id = idsFull.get(i);
                    if (names.get(id).toLowerCase().trim().contains(constraint.toString().toLowerCase().trim())) {
                        ids.add(id);
                    }
                }
            }
            return null;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            notifyDataSetChanged();
        }
    };

    @NonNull
    @Override
    public ProfileHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.cell_profile, parent, false);
        return new ProfileHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ProfileHolder holder, int position) {
        position = ids.get(position);
        setImageFromPath(imgs.get(position), holder.img);
        setTextData(holder.name, "Имя : " + names.get(position));
        setTextData(holder.email, "Email : " + emails.get(position));
        setTextData(holder.registrationDate, "Дата регистрации : " + dates.get(position));
        holder.idData = position;
    }

    /**
     * Устанавливает изображение в интерфейсный компонент
     * ImageView при помощи библиотеки Glide
     *
     * @param path путь к изображению
     * @param img  интерфейсный компонент ImageView
     */
    private void setImageFromPath(String path, ImageView img) {
        Glide.with(context).load(Uri.parse("file:///android_asset/images/photo.png")).into(img);
    }

    /**
     * Изменяет содержимое текстового поля
     *
     * @param textView текстовое поле
     * @param string   строка, которую необходимо добавить
     */
    private void setTextData(TextView textView, String string) {
        textView.setText(string);
    }

    @Override
    public int getItemCount() {
        return ids.size();
    }

    public class ProfileHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private Integer idData;
        private TextView name;
        private TextView email;
        private TextView registrationDate;
        private ImageView img;

        ProfileHolder(View itemView) {
            super(itemView);
            img = itemView.findViewById(R.id.image_model);
            name = itemView.findViewById(R.id.book_name);
            email = itemView.findViewById(R.id.author_name);
            registrationDate = itemView.findViewById(R.id.genre_name);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            Intent intent = new Intent(context, activity.getClass());
            intent.putExtra(DATABASE_ID, idData);
            context.startActivity(intent);
        }
    }
}


