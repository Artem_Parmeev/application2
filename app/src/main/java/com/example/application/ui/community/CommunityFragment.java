package com.example.application.ui.community;

import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.SearchView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.application.CommunityAdapter;
import com.example.application.DataBaseHelper;
import com.example.application.ProfileActivity;
import com.example.application.R;
import com.example.application.constants.DBConstants;

/**
 * Класс, описывающий фрагмент,
 * отображающий библиотеку
 *
 * @author Parmeev A.V. 17IT17
 */
public class CommunityFragment extends Fragment implements SearchView.OnQueryTextListener, DBConstants {

    private CommunityAdapter libraryAdapter;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_community, container, false);
        initBooksList(root);
        return root;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.search, menu);
        MenuItem menuItem = menu.findItem(R.id.search);

        SearchView searchView = (SearchView) menuItem.getActionView();
        searchView.setOnQueryTextListener(this);
    }

    /**
     * Инициализирует компоненты фрагмента для отображения
     * книг
     *
     * @param root базовый интерфейсный блок
     */
    private void initBooksList(View root) {
        DataBaseHelper dbHelper = new DataBaseHelper(root.getContext());
        String query = "select id, name, email, registrationDate from " + DATABASE_CLIENTS;
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        RecyclerView recyclerView = root.findViewById(R.id.books_list);
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(root.getContext());
        recyclerView.setLayoutManager(layoutManager);
        libraryAdapter = new CommunityAdapter(root.getContext(), db, query, null, new ProfileActivity());
        recyclerView.setAdapter(libraryAdapter);
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        libraryAdapter.getFilter().filter(newText);
        return false;
    }
}