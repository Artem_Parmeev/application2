package com.example.application;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;
import com.example.application.constants.DBConstants;

/**
 * Класс, представляющий собой активность, которая
 * отображает подробные данные о выбранной книге
 *
 * @author Parmeev A.V. 17IT17
 */
public class ProfileActivity extends AppCompatActivity implements View.OnClickListener, DBConstants {

    private SQLiteDatabase db;
    private Button btnToFriends;
    private int arg;
    private String email;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        email = getSharedPreferences("AccData", Context.MODE_PRIVATE).getString(DATABASE_EMAIL, null);
        ImageView imageView = findViewById(R.id.image_model_big);
        TextView textView = findViewById(R.id.data);
        Intent intent = getIntent();
        DataBaseHelper dbHelper = new DataBaseHelper(this);
        btnToFriends = findViewById(R.id.btn_to_friends);
        btnToFriends.setOnClickListener(this);
        db = dbHelper.getWritableDatabase();
        arg = intent.getIntExtra("id", 0);
        String[] data = getApplication().getResources().getStringArray(R.array.data);
        Cursor cursor = db.rawQuery("select id, name, email, registrationDate, userData, authorsData, booksData from " + DATABASE_CLIENTS + " where " + DATABASE_ID + " = " + arg, null);
        if (cursor.moveToFirst()) {
            String dataTextView;
            String tmp;
            for (int i = 1; i < cursor.getColumnCount(); i++) {
                dataTextView = textView.getText().toString();
                tmp = cursor.getString(cursor.getColumnIndex(cursor.getColumnNames()[i]));
                if (i == 1) {
                    setTitle(tmp);
                }
                if (i < 7) {
                    updateText(textView, dataTextView, data[i - 1], tmp);
                }
                setImage(imageView, tmp);
            }
        }
        cursor.close();
    }

    /**
     * Обновляет данные текстового поля
     *
     * @param textView     текстовое поле
     * @param dataTextView данные текстового поля
     * @param parameter    строка, относительно которой сформируется
     *                     добавляемый текст
     * @param data         добавляемые данные
     */
    private void updateText(TextView textView, String dataTextView, String parameter, String
            data) {
        String tmp = String.format(parameter, data);
        dataTextView = dataTextView.concat(tmp);
        textView.setText(dataTextView);
    }

    /**
     * Устанавливает изображение в интерфейсный компонент
     * ImageView
     *
     * @param imageView интерфейсный компонент, отображающий
     *                  изображение
     * @param path      путь к изображению
     */
    private void setImage(ImageView imageView, String path) {
        Glide.with(getApplicationContext()).load(Uri.parse("file:///android_asset/images/photo.png")).into(imageView);
    }

    @Override
    public void onClick(View v) {
        if (btnToFriends.equals(v)) {
            if (email == null) {
                return;
            }
            Cursor cursor = db.rawQuery("select * from " + DATABASE_FRIENDS, null);
            if (!isFoundSame(cursor, email, arg)) {
                ContentValues values = new ContentValues();
                values.put(DATABASE_ID, arg);
                values.put(DATABASE_EMAIL, email);

                db.insertWithOnConflict(DATABASE_FRIENDS, null, values, SQLiteDatabase.CONFLICT_NONE);
                Toast.makeText(this, "Пользователь добавлен в друзья", Toast.LENGTH_SHORT).show();
                setColor();
            }
        }
    }

    private boolean isFoundSame(Cursor cursor, String email, int arg) {
        boolean flag = false;
        if (cursor.moveToFirst()) {
            int idIndex = cursor.getColumnIndex(DATABASE_ID);
            int emailIndex = cursor.getColumnIndex(DATABASE_EMAIL);
            do {
                if (cursor.getInt(idIndex) == arg && cursor.getString(emailIndex).equals(email)) {
                    flag = true;
                    break;
                }
            } while (cursor.moveToNext());
        }
        cursor.close();
        return flag;
    }

    private void setColor() {
        btnToFriends.setBackgroundColor(getResources().getColor(R.color.grey));
        btnToFriends.setTextColor(getResources().getColor(R.color.bright_grey));
        btnToFriends.setText("В друзьях");
    }
}
