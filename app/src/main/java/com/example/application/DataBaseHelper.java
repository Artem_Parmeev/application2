package com.example.application;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

import com.example.application.constants.DBConstants;

/**
 * Класс, представляющий собой базу данных, содержащая
 * в себе некоторые таблицы
 *
 * @author Parmeev A.V. 17IT17
 */
public class DataBaseHelper extends SQLiteOpenHelper implements DBConstants {
    private Context context;

    public DataBaseHelper(@Nullable Context context) {
        super(context, "library", null, 1);
        this.context = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("create table " + DATABASE_CLIENTS + " (" + DATABASE_ID + " integer primary key autoincrement,"
                + DATABASE_NAME + " text,"
                + DATABASE_EMAIL + " text,"
                + DATABASE_PASSWORD + " text, userData text, authorsData text, booksData text, registrationDate text);");

        db.execSQL("create table " + DATABASE_FRIENDS + " (" + DATABASE_ID + " integer NOT NULL,"
                + DATABASE_EMAIL + " text NOT NULL"
                + ")");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
