package com.example.application;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;
import com.example.application.constants.DBConstants;

/**
 * Класс, представляющий собой активность, которая
 * отображает подробные данные о выбранном пользователе
 *
 * @author Parmeev A.V. 17IT17
 */
public class UserProfileActivity extends AppCompatActivity implements View.OnClickListener, DBConstants {

    private int arg;
    private SQLiteDatabase db;
    private Button btnToClose;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_profile);
        ImageView imageView = findViewById(R.id.image_model_big);
        TextView textView = findViewById(R.id.data);
        btnToClose = findViewById(R.id.btn_to_close);
        btnToClose.setOnClickListener(this);
        Intent intent = getIntent();
        DataBaseHelper dbHelper = new DataBaseHelper(this);
        db = dbHelper.getWritableDatabase();
        arg = intent.getIntExtra("id", 0);
        String[] data = getApplication().getResources().getStringArray(R.array.data);
        Cursor cursor = db.rawQuery("select id, name, email, registrationDate, userData, authorsData, booksData from " + DATABASE_CLIENTS + " where " + DATABASE_ID + " = " + arg, null);
        if (cursor.moveToFirst()) {
            String dataTextView;
            String tmp;
            for (int i = 1; i < cursor.getColumnCount(); i++) {
                dataTextView = textView.getText().toString();
                tmp = cursor.getString(cursor.getColumnIndex(cursor.getColumnNames()[i]));
                if (i == 1) {
                    setTitle(tmp);
                }
                if (i < 7) {
                    updateText(textView, dataTextView, data[i - 1], tmp);
                }
                setImage(imageView, tmp);
            }
        }
        cursor.close();
    }

    private void setColor() {
        btnToClose.setBackgroundColor(getResources().getColor(R.color.grey));
        btnToClose.setTextColor(getResources().getColor(R.color.bright_grey));
        btnToClose.setText("Пользователь удален");
    }

    @Override
    public void onClick(View v) {
            db.delete(DATABASE_FRIENDS, "id = ?", new String[]{String.valueOf(arg)});
            Toast.makeText(getApplicationContext(), "Пользователь удален из друзей", Toast.LENGTH_SHORT).show();
            btnToClose.setClickable(false);
            setColor();
    }

    /**
     * Обновляет данные текстового поля
     *
     * @param textView     текстовое поле
     * @param dataTextView данные текстового поля
     * @param parameter    строка, относительно которой сформируется
     *                     добавляемый текст
     * @param data         добавляемые данные
     */
    private void updateText(TextView textView, String dataTextView, String parameter, String
            data) {
        String tmp = String.format(parameter, data);
        dataTextView = dataTextView.concat(tmp);
        textView.setText(dataTextView);
    }

    /**
     * Устанавливает изображение в интерфейсный компонент
     * ImageView
     *
     * @param imageView интерфейсный компонент, отображающий
     *                  изображение
     * @param fileName  путь к изображению
     */
    private void setImage(ImageView imageView, String fileName) {
        Glide.with(getApplicationContext()).load(Uri.parse("file:///android_asset/images/photo.png")).into(imageView);
    }
}
